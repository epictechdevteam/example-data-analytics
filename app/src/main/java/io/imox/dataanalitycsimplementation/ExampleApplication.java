package io.imox.dataanalitycsimplementation;

import android.app.Application;

import java.util.concurrent.TimeUnit;

import io.imox.deviceinfo.DeviceInfo;

public class ExampleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        DeviceInfo.getInstance()
                .setIdBrand(8888)
                .setTimeRecolecData(1, TimeUnit.DAYS)
                .load(this);
    }
}
